﻿using Etier.IconHelper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Forms;
using System.Collections;

namespace TestProject
{
    
    
    /// <summary>
    ///This is a test class for IconListManagerTest and is intended
    ///to contain all IconListManagerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IconListManagerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for AddExtension
        ///</summary>
        ///
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\AddExtension.csv", "AddExtension#csv", DataAccessMethod.Sequential), DeploymentItem("TestProject\\CSV\\AddExtension.csv"), TestMethod()]
        public void AddExtensionTest()
        {
            string expException = Convert.ToString(TestContext.DataRow["Exception"]);
            try
            {
                ImageList imageList = new ImageList();
                IconReader.IconSize iconSize = new IconReader.IconSize();
                IconListManager target = new IconListManager(imageList, iconSize);
                string Extension = Convert.ToString(TestContext.DataRow["Extension"]);
                int ImageListPosition = Convert.ToInt32(TestContext.DataRow["ImageListPosition"]);
                target.AddExtension(Extension, ImageListPosition);
                Hashtable _extensionExpected = new Hashtable();
                _extensionExpected.Add(Extension, ImageListPosition);
                CollectionAssert.AreEqual(_extensionExpected, target._extensionList);
                //Assert.Inconclusive("Verify the correctness of this test method.");
                if (!String.IsNullOrWhiteSpace(expException))
                {
                    Assert.Fail("Expected exception -" + expException + " was not thrown");
                }
            }
            catch (Exception ex)
            {
                if (ex.GetType().Name.Equals(expException))
                {
                    
                }
                else
                {
                    Assert.Fail("Unexpected exception " + expException + " was thrown");
                }
            }
        }

        /// <summary>
        ///A test for AddFileIcon
        ///</summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\AddFileIcon.csv", "AddFileIcon#csv", DataAccessMethod.Sequential), DeploymentItem("TestProject\\CSV\\AddFileIcon.csv"), TestMethod()]
        public void AddFileIconTest()
        {
            string expException = Convert.ToString(TestContext.DataRow["Exception"]);

            try
            {
                
                ImageList imageList = new ImageList();
                IconReader.IconSize iconSize = IconReader.IconSize.Large;
                IconListManager target = new IconListManager(imageList, iconSize);
                string fileFolderPath = Convert.ToString(TestContext.DataRow["Path"]);
                string[] splitPath = fileFolderPath.Split(new Char[] { '.' });
                int expected = 0;
                int actual;
                actual = target.AddFileIcon(fileFolderPath);
                Assert.AreEqual(expected, actual);
                if (!String.IsNullOrWhiteSpace(expException))
                {
                    Assert.Fail("Expected exception -" + expException + " was not thrown");
                }
                //Assert.Inconclusive("Verify the correctness of this test method.");
            }
            catch (Exception ex)
            {
                if (ex.GetType().Name.Equals(expException))
                {
                    
                }
                else
                {
                    Assert.Fail("Unexpected exception " + expException + " was thrown");   
                }               
            }
       }
    }
}
