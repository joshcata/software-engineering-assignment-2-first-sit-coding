﻿using FileFindAndDeleteLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TestProject
{
    
    
    /// <summary>
    ///This is a test class for DateTimeComparerTest and is intended
    ///to contain all DateTimeComparerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class DateTimeComparerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for DateTimeComparer Constructor
        ///</summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\Compare.csv", "Compare#csv", DataAccessMethod.Sequential), DeploymentItem("TestProject\\CSV\\Compare.csv"), DeploymentItem("TestProject\\CSV\\Compare.csv"), TestMethod()]
        public void DateTimeComparerConstructorTest()
        {
            string expException = Convert.ToString(TestContext.DataRow["Exception"]);
            try
            {
                DateTimeComparer expectedDateTime = new DateTimeComparer();
                DateTime x = Convert.ToDateTime(TestContext.DataRow["Date x"]);
                DateTime y = Convert.ToDateTime(TestContext.DataRow["Date y"]);
                int actual;
                actual = expectedDateTime.Compare(x, y);
                Assert.AreEqual(x, y);
                //Assert.Inconclusive("Verify the correctness of this test method.");
                if (!String.IsNullOrWhiteSpace(expException))
                {
                    Assert.Fail("Expected exception -" + expException + " was not thrown");
                }
            }
            catch (Exception ex)
            {
                if (ex.GetType().Name.Equals(expException))
                {
                    
                }
                else
                {
                    Assert.Fail("Unexpected exception " + expException + " was thrown");
                }
            }
        }
    }
}
