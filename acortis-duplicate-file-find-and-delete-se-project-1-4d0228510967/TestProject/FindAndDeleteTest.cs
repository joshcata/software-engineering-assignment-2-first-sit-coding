﻿using FileFindAndDeleteLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace TestProject
{
    
    
    /// <summary>
    ///This is a test class for FindAndDeleteTest and is intended
    ///to contain all FindAndDeleteTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FindAndDeleteTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetFilesFromDirectory
        ///</summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\GetFilesFromDirectory.csv", "GetFilesFromDirectory#csv", DataAccessMethod.Sequential), DeploymentItem("TestProject\\CSV\\GetFilesFromDirectory.csv"), TestMethod()]
        public void GetFilesFromDirectoryTest()
        {
            string expException = Convert.ToString(TestContext.DataRow["Exception"]);
            try
            {
                string folder = Convert.ToString(TestContext.DataRow["Folder"]);
                string[] filters = new string[1];
                filters[0] = Convert.ToString(TestContext.DataRow["Filter"]);

                IEnumerable<FileInfo> filesGatheredFromApplication = null;

                filesGatheredFromApplication = FindAndDelete.GetFilesFromDirectory(folder, filters);

                string[] expectedFiles = Directory.GetFiles(folder, filters[0], SearchOption.AllDirectories).ToArray<string>();
                Array.Sort(expectedFiles);
                string[] actualFiles = new string[Convert.ToInt32(filesGatheredFromApplication.Count())];

                int i = 0; int count = 0;
                foreach (FileInfo file in filesGatheredFromApplication)
                {
                    actualFiles[i] = file.FullName;

                    i++;
                }
                i = 0;
                Array.Sort(actualFiles);
                foreach (FileInfo file in filesGatheredFromApplication)
                {
                    if (actualFiles[i].Equals(expectedFiles[i]))
                    {
                        count++;
                    }
                    i++;
                }

                Assert.AreEqual(count, filesGatheredFromApplication.Count());
                //Assert.Inconclusive("Verify the correctness of this test method.");
                if (!String.IsNullOrWhiteSpace(expException))
                {
                    Assert.Fail("Expected exception -" + expException + " was not thrown");
                }
            }
            catch (Exception ex)
            {
                if (ex.GetType().Name.Equals(expException))
                {
                    
                }
                else
                {
                    Assert.Fail("Unexpected exception " + expException + " was thrown");
                }
            }
        }
    }
}
