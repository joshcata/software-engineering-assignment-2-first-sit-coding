﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FileFindAndDeleteLibrary;
using Etier.IconHelper;

namespace DuplicateFileFindAndDelete
{
    public partial class MainForm : Form
    {
        private ImageList _smallImageList = new ImageList();
        private ImageList _largeImageList = new ImageList();
        private IconListManager _iconListManager;

        public MainForm()
        {
            InitializeComponent();
            
            OptionsComboBox.SelectedIndex = 0;
            FiltersComboBox.SelectedIndex = 5;
            
            _smallImageList.ColorDepth = ColorDepth.Depth32Bit;
            _largeImageList.ColorDepth = ColorDepth.Depth32Bit;

            _smallImageList.ImageSize = new System.Drawing.Size(16, 16);
            _largeImageList.ImageSize = new System.Drawing.Size(32, 32);

            _iconListManager = new IconListManager(_smallImageList, _largeImageList);

            FolderListView.SmallImageList = _smallImageList;
            FolderListView.LargeImageList = _largeImageList;
        }

        private List<string> selectedPaths = new List<string>();

        protected List<string> SelectedPaths
        {
            get
            {
                return selectedPaths;
            }
        }

        protected void AddSelectedPath(string newPath)
        {
            string folderPath = newPath.Substring(0, Math.Min(newPath.Length, 259));
            ListViewItem item = new ListViewItem(folderPath, _iconListManager.AddFolderIcon(folderPath));

            FolderListView.Items.Add(item);
            selectedPaths.Add(newPath);
        }

        protected void ClearAllPaths()
        {
            FolderListView.Items.Clear();
            selectedPaths.Clear();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            DialogResult browserResult = folderBrowserDialog.ShowDialog();

            switch (browserResult)
            {
                case DialogResult.OK:
                    AddSelectedPath(folderBrowserDialog.SelectedPath);
                    break;
                default:
                    break;
            }
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            ClearAllPaths();
        }

        object workingSemaphore = new object();
        bool isWorking = false;

        private void StartButton_Click(object sender, EventArgs e)
        {
            try
            {
                lock (workingSemaphore)
                {
                    if (isWorking)
                    {
                        MessageBox.Show("Error - you cannot find and delete two locations at once");
                        return;
                    }
                    else
                    {
                        isWorking = true;
                    }

                    StartButton.Enabled = false;

                    string filters = FiltersComboBox.Text.Split('|')[1].Trim();

                    FindAndDeleteOptions options;

                    switch (OptionsComboBox.Text)
                    {
                        case "Keep Older File Only":
                            options = FindAndDeleteOptions.KeepOlderFileOnly;
                            break;
                        case "Keep Newer File Only":
                            options = FindAndDeleteOptions.KeepNewerFileOnly;
                            break;
                        case "Create Duplicate File Report - Do Not Delete Anything":
                        default:
                            options = FindAndDeleteOptions.CreateDuplicateFileReportOnly;
                            break;
                    }

                    object[] arguments = new object[3];

                    arguments[0] = selectedPaths.ToArray<string>();
                    arguments[1] = filters;
                    arguments[2] = options;

                    backgroundWorker.RunWorkerAsync(arguments);
                }
            }
            catch (Exception ex)
            {
                StartButton.Enabled = true;
                throw ex;
            }
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            object[] arguments = (object[])e.Argument;

            string[] selectedPaths = (string[])arguments[0];
            string filters = (string)arguments[1];
            FindAndDeleteOptions options = (FindAndDeleteOptions)arguments[2];

            e.Result = FindAndDelete.FindAndDeleteFiles(selectedPaths, filters, options);
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            StartButton.Enabled = true;
            isWorking = false;

            if (e.Error != null)
            {
                MessageBox.Show("Unhandled Exception: " + e.Error.Message);
            }

            if (!e.Cancelled)
            {
                ReportForm rf = new ReportForm((string)e.Result);
                rf.Show();
                //MessageBox.Show((string)e.Result);
            }
            else
            {
                MessageBox.Show("Find and Delete Cancelled!");
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            bottomWebBrowser.DocumentText =
                "<html><head>" +
                "<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js\" type=\"text/javascript\"></script>" +
                "</head><body>" +
                "<!-- Begin: adBrite, Generated: 2012-05-16 16:33:56  --> " +
                "<script type=\"text/javascript\">" +
                "var AdBrite_Title_Color = '0000FF';" +
                "var AdBrite_Text_Color = '000000';" +
                "var AdBrite_Background_Color = 'FFFFFF';" +
                "var AdBrite_Border_Color = 'CCCCCC'; " +
                "var AdBrite_URL_Color = '008000'; " +
                "try{var AdBrite_Iframe=window.top!=window.self?2:1;var AdBrite_Referrer=document.referrer==''?document.location:document.referrer;AdBrite_Referrer=encodeURIComponent(AdBrite_Referrer);}catch(e){var AdBrite_Iframe='';var AdBrite_Referrer='';}" +
                "</script>" +
                "<span style=\"white-space:nowrap;\"><script type=\"text/javascript\">document.write(String.fromCharCode(60,83,67,82,73,80,84));document.write(' src=\"http://ads.adbrite.com/mb/text_group.php?sid=2152285&zs=3732385f3930&ifr='+AdBrite_Iframe+'&ref='+AdBrite_Referrer+'\" type=\"text/javascript\">');document.write(String.fromCharCode(60,47,83,67,82,73,80,84,62));</script>" +
                "<a target=\"_blank\" href=\"http://www.adbrite.com/mb/commerce/purchase_form.php?opid=2152285&afsid=1\"><img src=\"http://files.adbrite.com/mb/images/adbrite-your-ad-here-leaderboard.gif\" style=\"background-color:#CCCCCC;border:none;padding:0;margin:0;\" alt=\"Your Ad Here\" width=\"14\" height=\"90\" border=\"0\" /></a></span>" +
                "<!-- End: adBrite -->" +
                "<script>$(document).ready(function(){$(\"a\").attr('target','_blank');" +
                "$(\"body\").on(\"click\", \"a\", function(){" +
                "$(\"a\").attr('target','_blank'); return true;" +
                "});});</script>" +
                "</body></html>";

            sideWebBrowser.DocumentText =
                "<html><head>"+
                "<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js\" type=\"text/javascript\"></script>" +
                "</head><body>" +
                "<!-- Begin: adBrite, Generated: 2012-05-16 16:49:09  -->" +
                "<script type=\"text/javascript\">" +
                "var AdBrite_Title_Color = '0000FF';" +
                "var AdBrite_Text_Color = '000000';" +
                "var AdBrite_Background_Color = 'FFFFFF';" +
                "var AdBrite_Border_Color = 'CCCCCC';" +
                "var AdBrite_URL_Color = '008000';" +
                "try{var AdBrite_Iframe=window.top!=window.self?2:1;var AdBrite_Referrer=document.referrer==''?document.location:document.referrer;AdBrite_Referrer=encodeURIComponent(AdBrite_Referrer);}catch(e){var AdBrite_Iframe='';var AdBrite_Referrer='';}" +
                "</script>" +
                "<script type=\"text/javascript\">document.write(String.fromCharCode(60,83,67,82,73,80,84));document.write(' src=\"http://ads.adbrite.com/mb/text_group.php?sid=2152288&zs=3330305f323530&ifr='+AdBrite_Iframe+'&ref='+AdBrite_Referrer+'\" type=\"text/javascript\">');document.write(String.fromCharCode(60,47,83,67,82,73,80,84,62));</script>" +
                "<div><a target=\"_blank\" href=\"http://www.adbrite.com/mb/commerce/purchase_form.php?opid=2152288&afsid=1\" style=\"font-weight:bold;font-family:Arial;font-size:13px;\">Your Ad Here</a></div>" +
                "<!-- End: adBrite -->" +
                "<script>$(document).ready(function(){$('a[href^=\"http://\"]').live('click',function(){" +
                "$(this).attr('target','_blank'); return true;" +
                "});});</script>" +
                "</body></html>";

            paypalWebBrowser.DocumentText =
                "<form action=\"https://www.paypal.com/cgi-bin/webscr\" method=\"post\" target=\"_blank\">" +
                "<input type=\"hidden\" name=\"cmd\" value=\"_xclick\" />" +
                "<input type=\"hidden\" name=\"business\" value=\"andrew.cortis@gmail.com\" />" +
                "<input type=\"hidden\" name=\"item_name\" value=\"Donation\" />" +
                //"<input type=\"hidden\" name=\"amount\" value=\"5.00\" />" +
                "Like this program? <input type=\"image\" src=\"https://www.paypal.com/en_US/i/btn/btn_donate_LG.gif\" border=\"0\" name=\"submit\" alt=\"Make payments with PayPal - it's fast, free and secure!\">" +
                
                //    "<input type=\"image\" src=\"https://www.paypal.com/en_US/i/btn/x-click-but22.gif\" border=\"0\" name=\"submit\" alt=\"Make payments with PayPal - it's fast, free and secure!\">" +
            //<input type=\"submit\" value=\"Donate!\" />" +
                "</form>";

            fbWebBrowser.DocumentText =
                "<html xmlns:fb=\"http://ogp.me/ns/fb#\">\n" +
                "<body>\n" +
                "<div id=\"fb-root\"></div>\n" +
                "<script>\n" +
                "window.fbAsyncInit = function() {\n" +
                "FB.init({\n" +
                "appId      : '238558789577471',\n" +
                //"channelUrl : './Channel.htm',\n" +
                "status     : true, // check login status\n" +
                "cookie     : true, // enable cookies to allow the server to access the session\n" +
                "xfbml      : true  // parse XFBML\n" +
                "});\n" +
                "};\n" +
                "(function(d){\n" +
                "var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];\n" +
                "if (d.getElementById(id)) {return;}\n" +
                "js = d.createElement('script'); js.id = id; js.async = true;\n" +
                "js.src = \"http://connect.facebook.net/en_US/all.js\";\n" + // modified script here to choose http:// protocol over file:// protocol
                "ref.parentNode.insertBefore(js, ref);\n" +
                "}(document));\n" +
                "</script>\n" +
                "<div class=\"fb-like\" data-href=\"https://bitbucket.org/acortis/duplicate-file-find-and-delete/\" data-send=\"true\" data-layout=\"box_count\" data-width=\"450\" data-show-faces=\"true\"></div>" +
                "</body>\n</html>";

            sideWebBrowser.Navigating += new WebBrowserNavigatingEventHandler(Navigating);
            bottomWebBrowser.Navigating += new WebBrowserNavigatingEventHandler(Navigating);
            
            Timer t = new Timer();
            t.Interval = 24000;

            t.Tick += new EventHandler(t_Tick);
            t.Enabled = true;
            t.Start();
        }

        void Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            if (e.Url.OriginalString.StartsWith("http://click.adbrite.com"))
            {
                e.Cancel = true;

                OpenLink(e.Url.OriginalString);
            }
        }

        //http://www.devtoolshed.com/content/launch-url-default-browser-using-c
        public void OpenLink(string sUrl)
        {
            try
            {
                System.Diagnostics.Process.Start(sUrl);
            }
            catch (Exception exc1)
            {
                // System.ComponentModel.Win32Exception is a known exception that occurs when Firefox is default browser.  
                // It actually opens the browser but STILL throws this exception so we can just ignore it.  If not this exception,
                // then attempt to open the URL in IE instead.
                if (exc1.GetType().ToString() != "System.ComponentModel.Win32Exception")
                {
                    // sometimes throws exception so we have to just ignore
                    // this is a common .NET bug that no one online really has a great reason for so now we just need to try to open
                    // the URL using IE if we can.
                    try
                    {
                        System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo("IExplore.exe", sUrl);
                        System.Diagnostics.Process.Start(startInfo);
                        startInfo = null;
                    }
                    catch (Exception exc2)
                    {
                        // still nothing we can do so just show the error to the user here.
                    }
                }
            }
        }

        void t_Tick(object sender, EventArgs e)
        {
            sideWebBrowser.Navigating -= Navigating;

            bottomWebBrowser.Refresh(WebBrowserRefreshOption.Completely);
            sideWebBrowser.Refresh(WebBrowserRefreshOption.Completely);

            sideWebBrowser.Navigating += new WebBrowserNavigatingEventHandler(Navigating);
        }
    }
}
